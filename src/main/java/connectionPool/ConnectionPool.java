package connectionPool;

import java.util.Vector;

public class ConnectionPool {
    Vector<SqlConn>pool;

    public ConnectionPool() {
        this.pool = new Vector<>();
        for (int i=0;i<5;i++){
            SqlConn connection=new SqlConn();
            connection.start();
            this.pool.add(connection);
        }
    }

    private void increasePool(){
        System.out.println("increasing pool");
        SqlConn connection=new SqlConn();
        connection.start();
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.pool.add(connection);
    }

    public void decreasePool(){
        if(pool.size()>5){
            for (int i=0;i<pool.size();i++){
                if(pool.get(i).getIsFree()){
                    pool.get(i).setClose();
                    pool.remove(i);
                }
            }
        }
    }

    public int getPoolSize(){
        return pool.size();
    }

    public SqlConn getSqlCon(){
        int i=0;
        for(SqlConn con:pool){
            if(con.getIsFree()){
                System.out.println(i);
                con.getCon();
                return con;
            }
            i++;
        }

        var poolSize = pool.size();
        if(poolSize<10){
        increasePool();
        while (poolSize==pool.size()){

        }
            return getSqlCon();
        }
       return null;
    }

    public int getActiveConnectionsCount(){
        int count=0;
        for(SqlConn sqlConn:pool){
            if(!sqlConn.getIsFree()){
                count++;
            }
        }
        return count;

    }
}
