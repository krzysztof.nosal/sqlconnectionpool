package connectionPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Vector;

public class Main {
    private final static String DBURL = "jdbc:mysql://localhost:3306/connectionpool";
    private final static String DBUSER = "root";
    private final static String DBPASS = "";
    private final static String DBDRIVER = "com.mysql.jdbc.Driver";

    public static void main(String[] args) {

       ConnectionPool connectionPool=new ConnectionPool();
       try {
           Thread.sleep(1000);
       }catch (Exception e){
           e.printStackTrace();
       }
       var tm = System.currentTimeMillis();
        System.out.println(tm);
       Vector<Worker>workers=new Vector<>();
       for(int i=0;i<10;i++){
           SqlConn connection=null;
           while (connection==null) {
               connection = connectionPool.getSqlCon();
           }
            Worker worker=new Worker(connection);
            worker.start();
            workers.add(worker);
       }
       boolean finished = false;
       while (!finished){
           System.out.println("activ conn: "+connectionPool.getActiveConnectionsCount());
           finished=true;
           for(Worker worker:workers){
               if(worker.isAlive()){
                   finished=false;
               }
           }
       }
       var tm2 = System.currentTimeMillis();
        System.out.println(tm2);
       var tm3=tm2-tm;
        System.out.println(tm3);
        System.out.println(connectionPool.getPoolSize());
        connectionPool.decreasePool();
        System.out.println(connectionPool.getPoolSize());

    }
}
