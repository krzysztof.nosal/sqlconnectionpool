package connectionPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.sql.DriverManager.getConnection;

public class SqlConn extends Thread {
    private final static String DBURL = "jdbc:mysql://localhost:3306/connectionpool";
    private final static String DBUSER = "root";
    private final static String DBPASS = "";
   private Connection con;
    private AtomicBoolean isFree;
private AtomicBoolean close;
    public boolean getIsFree() {
        return isFree.get();
    }

    public SqlConn() {
        isFree=new AtomicBoolean(true);
        close=new AtomicBoolean(false);

    }

    public Connection getCon(){
        isFree.set(false);
        return con;
    }

    public void setFree(){
        isFree.set(true);
    }

    public void setClose(){close.set(true);}
    @Override
    public synchronized void run() {
        try {
            this.con = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
            currentThread().sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }

        while (!close.get()){
            ;
        }
        try {
            con.close();
        }catch (Exception e){
            e.printStackTrace();
        }


    }




}
