package connectionPool;

import java.sql.Connection;
import java.sql.Statement;

public class Worker extends Thread{
    Statement statement;
    String query="INSERT INTO pooltest(value) VALUE (1);";
    SqlConn sqlConn;

    public Worker(SqlConn sqlConn) {
        this.sqlConn = sqlConn;
    }

    @Override
    public void run() {
        Connection connection=sqlConn.getCon();;


        try (Statement stmt = connection.createStatement()) {
            for(int i=0;i<10000;i++){
                stmt.executeUpdate(query);
            }


        }
        catch (Exception e){
            e.printStackTrace();
        }
        sqlConn.setFree();
    }
}
